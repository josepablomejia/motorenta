//
//  main.c
//  motorenta.c
//
//  Created by Jose Mejia on 12/2/19.
//  Copyright © 2019 Jose Mejia. All rights reserved.
//

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <curses.h>
#include <stdlib.h>

//----------------ESTRUCTURAS-------------//
struct m{
    int mID;
    float carga;
    char lugar[100];
    char mstatus[13];   //disponible/mantenimiento//
    char notas[100];
};

struct e{
    int sID;
    int motos[10];
    char nombre[30];
    char calle[30];
    char colonia[30];
    char cp[6];
    int numero;
    char estatus[10];    //abierta/cerrada//
};

struct u{
    int uID;
    char nombre[100];
    char apellidop[100];
    char apellidom[100];
    char clave[100];
    char ustatus[15];
};

struct a{
    char aID[5];
    char nombre[100];
    char clave[100];
};

//------------VARIABLES GLOBALES----------//
int numestaciones=0; int numusuarios=0;int nummotos=0;int numadmins=0;
int idestaciones=3000; int idusuarios=2000; int idmotos=1000; int idadmins=0;
int opcion,temp;
int x, y, z;
char op[50];
struct m motos[100];
struct e estaciones[100];
struct u usuarios[200];
struct a admins[3];
char id[5];
char clave[20];
char buscar[20];

char a[5]="0001";
char b[10]="mejia";
char c[10]="123";
//-----------------POINTERS---------------//

FILE *fpm;
FILE *fpe;
FILE *fpu;
FILE *fpa;

//-----------------FUNCIONES--------------//
void usuario(void);
void renta(void);
void display(void);

void admin(void);
void agregaru(void);
void eliminaru(void);
void directorio(void);
void funcionestaciones(void);
void cerrarestacion(void);
void abrirestaciones(void);
void eliminarestacion(void);

//------------------------------------INICIO----------------------------------//
int main(void)
{
    fpm = fopen("motos.txt", "r");
    fpe = fopen("estaciones.txt", "r");
    fpu = fopen("usuarios.txt", "r");
    fpa = fopen("admins.txt","r");
    
    
    strcpy(admins[0].aID,a);
    strcpy(admins[0].nombre,b);
    strcpy(admins[0].clave,c);
    
    fread(admins, sizeof(admins), 1, fpa);
    fread(usuarios, sizeof(usuarios), 1, fpu);
    fread(estaciones, sizeof(estaciones), 1, fpe);
    fread(motos, sizeof(motos), 1, fpm);
    
    
       fclose(fpm);
       fclose(fpe);
       fclose(fpu);
       fclose(fpa);
    
   
    
    printf(" \n\n      BIENVENIDO A MOTORENTA       ");
    printf("\n\n\n          Alexander Attie\n        Jose Pablo Mejia");
    printf("\n \n \n");
    printf("\n          ID:");
    scanf("%s",id);
    printf("\n    Password:");
    scanf("%s",clave);
    
        for(y=0; y<400; y++)
        {
            if((strcmp(id,admins[y].aID)==0) && (strcmp(clave,admins[y].clave)==0))
            {
                // contar numero de usuarios //
                   for(x=0;x<200;x++)
                   {
                       if((strcmp(usuarios[x].ustatus,"habilitado")==0))
                       {
                           numusuarios=numusuarios+1;
                           idusuarios=idusuarios+1;
                       }
                       else if((strcmp(usuarios[x].ustatus,"deshabilitado")==0))
                       {
                           idusuarios=idusuarios+1;

                       }
            
                       
                   }
                   
                   //contar numero de estaciones
                   for(x=0;x<100;x++)
                   {
                       if((strcmp(estaciones[x].estatus,"abierta")==0))
                       {
                           numestaciones=numestaciones+1;
                           idestaciones=idestaciones+1;
                       }
                       else if((strcmp(estaciones[x].estatus,"cerrada")==0))
                       {
                           idestaciones=idestaciones+1;
                       }
                 
                        
                   }
                    system("clear");
                
                   printf("%d\n %d nE%d IdE%d\n ", numusuarios, idusuarios, numestaciones, idestaciones);
                admin();
            }
                
            else
            {
                system("clear");
                printf("\n *** Usuario o clave erroneos, por favor intentelo de nuevo ***\n\n");
                printf("%s %s\n", clave, id);
                main();
            }
    
        }
    
    fpm = fopen("motos.txt", "wt");
    fpe = fopen("estaciones.txt", "wt");
    fpu = fopen("usuarios.txt", "wt");
    fpa = fopen("admins.txt","wt");
    
    fwrite(admins, sizeof(admins), 1, fpa);
    fwrite(usuarios, sizeof(usuarios), 1, fpu);
    fwrite(motos, sizeof(motos), 1, fpm);
    fwrite(estaciones, sizeof(estaciones), 1, fpe);
    
    fclose(fpm);
    fclose(fpe);
    fclose(fpu);
    fclose(fpa);
    
   
    
    return 0;
    
}





void admin(void)
{
    
    printf("   ------------------------------");
    printf("\n  |       MENU ADMINISTRADOR     |\n");
    printf("   ------------------------------ \n \n");

    printf("    1-Agregar Usuario\n\n    2-Eliminar Usuario\n\n    3-Motos\n\n    4-Estaciones\n\n    5-Editar Operacion\n\n    6-Mostrar Operacion\n\n    7-Directorio\n\n    8-Salir\n\n");
        
        opcion=0;
    
    printf("Ir a:");
    scanf("%d", &opcion);
    
    if(opcion == 1)
    {
        system("clear");
        agregaru();
    }
    else if(opcion == 2)
    {
        system("clear");
        eliminaru();
    }
    else if(opcion == 4)
    {
        system("clear");
        funcionestaciones();
    }
    else if(opcion == 7)
    {
        system("clear");
        directorio();
    }
    else if(opcion == 8)
    {
        printf("Hasta Luego\n\n");
    }
    else
    {
        printf("Opcion no reconocida\n");
        admin();
    }
    
}










void agregaru(void)
{
    
    printf("   ------------------------------");
    printf("\n   |     MENU AGREGAR USUARIO     |\n");
    printf("   ------------------------------ \n \n ");
    
    numusuarios=numusuarios+1;
    idusuarios=idusuarios+1;
    usuarios[numusuarios].uID=idusuarios;
    
    printf(" uID: %d", usuarios[numusuarios].uID);
    printf("\n Nombre:");
    scanf("%s",usuarios[numusuarios].nombre);
    printf("\n Apellido P:");
    scanf("%s",usuarios[numusuarios].apellidop);
    printf("\n Apelldio M:");
    scanf("%s",usuarios[numusuarios].apellidom);
    printf("\n Clave:");
    scanf("%s",usuarios[numusuarios].clave);
    
    
    printf("\n Los datos estan correctos?\n");
    printf("\n1- si                   2-no\n");
    
    opcion=0;

    
    printf(":");
    scanf("%d", &opcion);
    
    if(opcion == 1)
    {
        strcpy(usuarios[numusuarios].ustatus,"habilitado");
        system("clear");
        printf("**** GUARDADO EXITOSAMENTE ****\n");
        admin();
        opcion=0;
    }
    else
    {
        numusuarios=numusuarios-1;
        idusuarios=idusuarios-1;
        
        agregaru();
        opcion=0;
    }

}

void eliminaru(void)
{
       printf("   ------------------------------");
       printf("\n   |     MENU ELIMINAR USUARIO     |\n");
       printf("   ------------------------------ \n \n ");
    
    opcion=0;

    
    printf("Que Id de usuario desea borrar:");
    scanf("%d", &opcion);
    
    for(x=0; x<=numusuarios; x++)
    {
        if(opcion == usuarios[x].uID)
        {
            system("clear");
            
            printf("\n  uId: %d\n\n  Nombre: %s %s %s\n ", usuarios[x].uID,usuarios[x].apellidop, usuarios[x].apellidom, usuarios[x].nombre);
            
            printf("\n    ***Eliminar?*** \n     1-si    2-no\n");
            
            opcion=0;

            
            printf(":");
            scanf("%d", &opcion);
            
            if(opcion==1)
            {
                strcpy(usuarios[x].ustatus,"deshabilitado");
                opcion=0;
           
            }
            else
            {
                eliminaru();
                opcion=0;
            }
        }
        else
        {
            
        }
    }
    system("clear");
    printf("**** ElIMINADO EXITOSAMENTE ****");
    admin();
}

void directorio(void)
{
    system("clear");
    
    printf("   ------------------------------");
    printf("\n   |       DIRECTORIO           |\n");
    printf("   ------------------------------ \n\n\n\n ");
    
    x=0;
    
    printf("uID     Nombre\n");
    while(x<(numusuarios+1))
    {
        if((strcmp(usuarios[x].ustatus,"habilitado")==0))
        {
        printf("\n%d  %s %s %s", usuarios[x].uID, usuarios[x].apellidop, usuarios[x].apellidom, usuarios[x].nombre);
            
            x=x+1;
        }
        else
        {
            x=x+1;
        }
    }
    opcion=0;
    
    printf("\n\n\nPresione 1 para regresar:\n");
    scanf("%d", &opcion);
    
    if(opcion == 1)
    {
        system("clear");
        admin();
        opcion=0;
    }
    else
    {
        system("clear");
        directorio();
        opcion=0;
    }
}




void funcionestaciones(void)
{
       
       printf("   ------------------------------");
       printf("\n   |       MENU ESTACIONES         |\n");
       printf("   ------------------------------ \n\n\n\n");
    
    printf("   1-Abrir Estacion\n\n");
    printf("   2-Cerrar Estacion\n\n");
    printf("   3-Crear Estacion\n\n");
    printf("   4-Eliminar estacion\n\n");
    printf("   5-Mostrar todas\n\n");
    printf("   6-Regresar\n\n");
    
    
    opcion=0;
    
    printf("Seleccione opcion:");
       scanf("%d", &opcion);
    
    if(opcion==1)
    {
        
        system("clear");
        abrirestaciones();
        
    }
    
    else if(opcion==2)
    {
        
        system("clear");
        cerrarestacion();
        
    }
    
    else if(opcion==3)
    {
        system("clear");
        
        printf("   ----------------------------------");
        printf("\n   |           CREAR ESTACION        |\n");
        printf("   ---------------------------------- \n\n\n\n ");
    
        numestaciones=numestaciones+1;
        idestaciones=idestaciones+1;
        
        estaciones[numestaciones].sID=idestaciones;
        
        printf(" eID: %d\n", estaciones[numestaciones].sID);
        printf("\n  DIRECCION DESEADA\n");
        printf("\n  Calle:");
        scanf("%s",estaciones[numestaciones].calle);
        printf("\n  Numero:");
        scanf("%d",&estaciones[numestaciones].numero);
        printf("\n  Colonia:");
        scanf("%s",estaciones[numestaciones].colonia);
        printf("\n  Codigo Postal:");
        scanf("%s", estaciones[numestaciones].cp);
        printf("\n  Nombre Estacion:");
        scanf("%s",estaciones[numestaciones].nombre);
        for(x=0;x<10;x++)
        {
            estaciones[numestaciones].motos[x]=0000;
        }
        strcpy(estaciones[numestaciones].estatus,"abierta");
        printf("Estatus: %s\n", estaciones[numestaciones].estatus);
        
        printf("\n\n ** Estan Correctos los Datos? **\n\n ");
        
        opcion=0;

        
        printf("     1- si    2-no\n");
        scanf("%d", &opcion);
        
            if(opcion==1)
            {
                system("clear");
                printf(" *** Estacion %d %s creada con exito\n", estaciones[numestaciones].sID, estaciones[numestaciones].nombre);
                funcionestaciones();
                opcion=0;
            }
        else if(opcion ==2)
            {
                numestaciones=numestaciones-1;
                idestaciones=idestaciones-1;
                
                system("clear");
                printf(" *** Operacion Cancelada **");
                funcionestaciones();
                opcion=0;
            }
        opcion=0;
    }
    
    
    
    else if(opcion==4)
    {
        system("Clear");
        eliminarestacion();
    }
    
    
    
    else if(opcion==5)
    {
        system("clear");
    
        printf("   ---------------------------------");
        printf("\n   |       DIRECTORIO ESTACIONES        |\n");
        printf("   --------------------------------- \n\n\n\n");
        
        for(x=0; x<=numestaciones; x++)
        {
            
            if((estaciones[x].sID>3000)&&(strcmp(estaciones[x].estatus,"eliminada")!=0))
            {
                printf(" %d  Estatus:%s\n %s\n %s, %d, %s, %s\n",estaciones[x].sID, estaciones[x].estatus, estaciones[x].nombre, estaciones[x].calle, estaciones[x].numero, estaciones[x].colonia, estaciones[x].cp );
                for(y=0;y<10;y++)
                {
                    printf(" %d",estaciones[x].motos[y] );
                }
                
                printf("\n\n");
            }
        }
        
            opcion=0;
        
            printf("\n\n\nPresione 1 para regresar:\n");
            scanf("%d", &opcion);
        
            if(opcion == 1)
            {
                system("clear");
                funcionestaciones();
                opcion=0;
            }
            else
            {
                system("clear");
                funcionestaciones();
                opcion=0;
            }
        
        
    }
    
    
    else if(opcion==6)
    {
        system("clear");
        admin();
        opcion=0;
    }
       
}

void cerrarestacion(void)
{
    printf("   ---------------------------------");
    printf("\n   |       CERRAR ESTACIONES        |\n");
    printf("   --------------------------------- \n\n\n\n");
    
    printf("Nombre:");
    scanf("%s", buscar);
    
    for(x=0; x<=numestaciones; x++)
    {
        if((strcmp(buscar, estaciones[x].nombre)==0))
           {
               printf("Esta es la estacion deseada?\n\n");
               
               printf("ID: %d\n %s %s Estatus:%s\n\n",estaciones[x].sID, estaciones[x].nombre, estaciones[x].calle, estaciones[x].estatus);
               
               opcion =0;
               printf("\n1-si/cerrar   2-no/cancelar\n");
               scanf("%d", &opcion);
               
               if(opcion==1)
               {
                   strcpy(estaciones[x].estatus,"cerrada");
                   system("clear");
                   printf("*** Estacion: %s Estatus: %s ***\n", estaciones[x].nombre, estaciones[x].estatus);
                   
                   funcionestaciones();
                   
               }
               else
               {
                   system("clear");
                              
                    printf("*** Operacion Cancelada***\n");
                              
                    funcionestaciones();
               }
               
               x=numestaciones;
           }
        
        else if(x==numestaciones)
        {
            system("clear");
            
          printf("Estacion no encontrada\n");
            
            funcionestaciones();
            
        }
    }
    
}


void abrirestaciones(void)
{
    printf("   ---------------------------------");
    printf("\n   |       ABRIR ESTACIONES        |\n");
    printf("   --------------------------------- \n\n\n\n");
    
    printf("Nombre:");
    scanf("%s", buscar);
    
    for(x=0; x<=numestaciones; x++)
    {
        if((strcmp(buscar, estaciones[x].nombre)==0))
           {
               printf("Esta es la estacion deseada?\n\n");
               
               printf("ID: %d\n %s %s Estatus:%s\n\n",estaciones[x].sID, estaciones[x].nombre, estaciones[x].calle, estaciones[x].estatus);
               
               opcion =0;
               printf("\n1-si/abrir   2-no/cancelar\n");
               scanf("%d", &opcion);
               
               if(opcion==1)
               {
                   strcpy(estaciones[x].estatus,"abierta");
                   system("clear");
                   printf("*** Estacion: %s Estatus: %s ***\n", estaciones[x].nombre, estaciones[x].estatus);
                   
                   funcionestaciones();
                   
               }
               else
               {
                   system("clear");
                              
                    printf("*** Operacion Cancelada***\n");
                              
                    funcionestaciones();
               }
               
               x=numestaciones;
           }
        
        else if(x==numestaciones)
        {
            system("clear");
            
          printf("Estacion no encontrada\n");
            
            funcionestaciones();
            
        }
    }
}


void eliminarestacion(void)
{
    printf("   ------------------------------");
    printf("\n   |      ELIMINAR ESTACION     |\n");
    printf("   ------------------------------ \n \n ");
       
       opcion=0;

       
       printf("Que estacion desea borrar:");
       scanf("%s", buscar);
       
       for(x=0; x<=numestaciones; x++)
       {
           if((strcmp(estaciones[x].nombre,buscar)==0))
           {
               system("clear");
               
               printf("Esta es la estacion deseada?\n\n");
               
               printf("ID: %d\n %s %s Estatus:%s\n\n",estaciones[x].sID, estaciones[x].nombre, estaciones[x].calle, estaciones[x].estatus);
               
               printf("\n si-Eliminar    no-Cancelar\n");
               
               opcion=0;

               scanf("%d", &opcion);
               
               if(opcion==1)
               {
                   strcpy(estaciones[x].estatus,"eliminada");
                   opcion=0;
                   
                   system("clear");
                   
                   printf("*** Estacion: %s eliminada correctamente***\n", estaciones[x].nombre);
                   
                   funcionestaciones();
              
               }
               else
               {
                   system("clear");
                   printf("**** OPERACION CANCELADA ****");
               }
           }
        
       }
       
}
